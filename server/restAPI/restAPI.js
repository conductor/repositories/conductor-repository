// Global configuration
Api = new Restivus({
  prettyJson: true,
  apiPath: 'api/',
  defaultHeaders: {
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Headers': 'origin, content-type, accept'
  }
});

Api.addRoute('search', {}, {
  post: {
    action: function(data) {
      console.log(this.bodyParams);

      if (this.bodyParams.simpleQuery) {
        console.log('test');
        var query = this.bodyParams.simpleQuery;

        return {
          devices: SearchDevice.simpleQuery(query),
          componentGroups: SearchComponentGroup.simpleQuery(query)
        };
      }
    }
  },
  options: {
    action: function() {
      return '';
    }
  }
});

Api.addRoute('download/:org/:name/:version', {}, {
  post: {
    action: function(data) {
      console.log("Incoming request: " + JSON.stringify(this.urlParams));

      if (!this.urlParams.org || !this.urlParams.name || !this.urlParams.version) {
        throw new Meteor.Error(400, 'Missing parameters, you must specify "org", "name" and "version" for the file you want to download');
      }

      var query = {
        org: {
          $regex: this.urlParams.org
        },
        name: {
          $regex: this.urlParams.name
        },
        version: {
          $regex: this.urlParams.version
        }
      };

      var componentGroup = ComponentGroups.findOne(query);

      if (!componentGroup) {
        throw new Meteor.Error(404, 'Couldn\'t find any jar file for download.');
      }

      var file = JarRepository.findOne(componentGroup.fileId);
      var fileDownloadURL = Meteor.settings.cfsBasePath + '/files/jarRepository/' + file._id + '/' + file.original.name; // Let the cfs package handle the file download instead.

      this.response.writeHead(302, {
        'Location': fileDownloadURL
      });

      this.done();
      return {};
    }
  },
  options: {
    action: function() {
      return '';
    }
  }
});
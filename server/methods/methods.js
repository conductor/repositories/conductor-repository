Meteor.methods({
    addDevice: function(device) {
        if (!this.userId) {
            throw new Meteor.Error(401, 'You must be logged in to add a device.');
        }

        DeviceValidator.validate(device);

        if (DeviceRepository.exist(device.org, device.name, device.version)) {
            throw new Meteor.Error(400, 'A device already exist with the same org, name and version.');
        }

        device.org = User.getUser(this.userId).org;
        device.package = device.org + ':' + device.name + ':' + device.version

        // Add the whole component group with all its properties to the device
        device.componentGroups = _.map(device.componentGroups, function(componentGroup) {
            return ComponentGroupRepository.find(componentGroup.org, componentGroup.name, componentGroup.version);
        });

        Devices.insert(device);
    },
    addComponentGroup: function(componentGroup) {
        if (!this.userId) {
            throw new Meteor.Error(401, 'You must be logged in to add a componet group.');
        }

        ComponentGroupValidator.validate(componentGroup);

        componentGroup.org = User.getUser(this.userId).org;

        if (ComponentGroupRepository.exist(componentGroup.org, componentGroup.name, componentGroup.version)) {
            throw new Meteor.Error(400, 'A component group already exist with the same org, name and version.');
        }

        componentGroup.package = componentGroup.org + ':' + componentGroup.name + ':' + componentGroup.version

        ComponentGroups.insert(componentGroup);
    },
    searchDevice: function(query) {
        console.log(query);
        if (query.simpleQuery) {
            return SearchDevice.simpleQuery(query.simpleQuery);
        } else {
            return SearchDevice.detailedQuery(query);
        }
    },
    searchComponentGroup: function(query) {
        if (query.simpleQuery) {
            return SearchComponentGroup.simpleQuery(query.simpleQuery);
        } else {
            return SearchComponentGroup.detailedQuery(query);
        }
    },
    downloadURL: function(componentGroup) {
        var group = ComponentGroups.findOne({
            org: componentGroup.org,
            name: componentGroup.name,
            version: componentGroup.version
        });

        if (!group) {
            throw new Meteor.Error(400, 'No component group exist with the org "' + componentGroup.org + '", name "' + componentGroup.name + '", version "' + componentGroup.version + '"');
        }

        var file = JarRepository.findOne(group.fileId);

        if (!file) {
            throw new Meteor.Error(400, 'No downloadable file was found for that component group');
        }

        return Meteor.settings.cfsBasePath + '/files/jarRepository/' + file._id + '/' + file.original.name;
    },
    getUploadProgress: function(id) {
        var file = JarRepository.findOne({
            _id: id
        });

        // If fully uploaded, return 100
        if (file.uploadedAt) {
            return 100;
        }
        // Otherwise return the confirmed progress or 0
        else {
            return Math.round((file.chunkCount || 0) / (file.chunkSum || 1) * 100);
        }

    }
});
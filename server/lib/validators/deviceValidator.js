DeviceValidator = (function() {
    function validate(device) {
        if (!DataTypeUtil.isString(device.title) || device.title.length === 0) {
            throw new Meteor.Error(400, 'Invalid device title. The name of the device cannot be empty.');
        }

        if (!DataTypeUtil.isString(device.description) || device.description.length === 0) {
            throw new Meteor.Error(400, 'Invalid device description. The device description cannot be empty.');
        }

        if (!DataTypeUtil.isString(device.name) || device.name.length === 0) {
            throw new Meteor.Error(400, 'Invalid device name. The device name cannot be empty.');
        }

        if (!DataTypeUtil.isString(device.type) || device.type.length === 0) {
            throw new Meteor.Error(400, 'Invalid device type. The device type cannot be empty.');
        }

        if (!DataTypeUtil.isString(device.version) || device.version.length === 0) {
            throw new Meteor.Error(400, 'Invalid device version. The device version cannot be empty.');
        }

        if (!DataTypeUtil.isArray(device.componentGroups)) {
            throw new Meteor.Error(400, 'Invalid component groups. The component groups must be an array.');
        }

        device.componentGroups.forEach(function(componentGroup) {
            if (!DataTypeUtil.isString(componentGroup.org) || componentGroup.org.length === 0) {
                throw new Meteor.Error(400, 'Invalid component group org. The component group org cannot be empty.');
            }

            if (!DataTypeUtil.isString(componentGroup.name) || componentGroup.name.length === 0) {
                throw new Meteor.Error(400, 'Invalid component group name. The component group name cannot be empty.');
            }

            if (!DataTypeUtil.isString(componentGroup.version) || componentGroup.version.length === 0) {
                throw new Meteor.Error(400, 'Invalid component group version. The component group version cannot be empty.');
            }

            if (!ComponentGroupRepository.exist(componentGroup.org, componentGroup.name, componentGroup.version)) {
                throw new Meteor.Error(400, 'No component group exist with org "' + componentGroup.org + '", name "' + componentGroup.name + '", version "' + componentGroup.version + '"');
            }
        });

    }

    return {
        validate: validate
    }
}());
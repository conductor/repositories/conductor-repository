SearchComponentGroup = (function() {
    function simpleQuery(query) {
        var queryParts = query.split(' ');
        var searchQuery = '';

        _.each(queryParts, function(queryPart, index) {
            if (index === 0) {
                searchQuery += '(?=.*' + queryPart + '.*)';
            } else {
                searchQuery += '(?=.*' + queryPart + '.*)';
            }
        });

        var hideFields = {
            fields: {
                _id: 0,
                fileId: 0
            }
        }

        return ComponentGroups.find({
            package: {
                $regex: searchQuery
            }
        }, hideFields).fetch();
    }

    function detailedQuery(detailedQuery) {
        var query = {};

        if (detailedQuery.org) {
            query['org'] = {
                $regex: '(?=.*' + detailedQuery.org + '.*)'
            };
        }

        if (detailedQuery.name) {
            query['name'] = {
                $regex: '(?=.*' + detailedQuery.name + '.*)'
            };
        }

        if (detailedQuery.version) {
            query['version'] = {
                $regex: '(?=.*' + detailedQuery.version + '.*)'
            };
        }

        if (detailedQuery.componentGroups && detailedQuery.componentGroups.name) {
            query['componentGroups.name'] = {
                $regex: '(?=.*' + detailedQuery.componentGroups.name + '.*)'
            };
        }

        if (detailedQuery.componentGroups && detailedQuery.componentGroups.org) {
            query['componentGroups.org'] = {
                $regex: '(?=.*' + detailedQuery.componentGroups.org + '.*)'
            };
        }

        if (detailedQuery.components && detailedQuery.components.name) {
            query['components.name'] = {
                $regex: '(?=.*' + detailedQuery.components.name + '.*)'
            };
        }

        if (detailedQuery.components && detailedQuery.components.type) {
            query['components.type'] = {
                $regex: '(?=.*' + detailedQuery.components.type + '.*)'
            };
        }

        var hideFields = {
            fields: {
                _id: 0,
                fileId: 0
            }
        }

        return ComponentGroups.find(query, hideFields).fetch();
    }

    return {
        simpleQuery: simpleQuery,
        detailedQuery: detailedQuery
    }
}());
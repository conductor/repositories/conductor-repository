SearchDevice = (function() {
    function simpleQuery(query) {
        var queryParts = query.split(' ');
        var searchQuery = '';

        _.each(queryParts, function(queryPart, index) {
            if (index === 0) {
                searchQuery += '(?=.*' + queryPart + '.*)';
            } else {
                searchQuery += '(?=.*' + queryPart + '.*)';
            }
        });

        var hideFields = {
            fields: {
                _id: 0,
                fileId: 0
            }
        }

        return Devices.find({
            package: {
                $regex: searchQuery
            }
        }, hideFields).fetch();
    }

    function detailedQuery(detailedQuery) {
        var query = {};

        if (detailedQuery.org) {
            query['org'] = {
                $regex: '(?=.*' + detailedQuery.org + '.*)'
            };
        }

        if (detailedQuery.name) {
            query['name'] = {
                $regex: '(?=.*' + detailedQuery.name + '.*)'
            };
        }

        if (detailedQuery.version) {
            query['version'] = {
                $regex: '(?=.*' + detailedQuery.version+ '.*)'
            };
        }

        if (detailedQuery.componentGroups && detailedQuery.componentGroups.name) {
            query['componentGroups.name'] = {
                $regex: '(?=.*' + detailedQuery.componentGroups.name + '.*)'
            };
        }

        if (detailedQuery.componentGroups && detailedQuery.componentGroups.org) {
            query['componentGroups.org'] = {
                $regex: '(?=.*' + detailedQuery.componentGroups.org + '.*)'
            };
        }

        var hideFields = {
            fields: {
                _id: 0,
                fileId: 0
            }
        }

        return Devices.find(query, hideFields).fetch();
    }

    return {
        simpleQuery: simpleQuery,
        detailedQuery: detailedQuery
    }
}());
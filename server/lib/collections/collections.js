JarRepository = new FS.Collection("jarRepository", {
    stores: [new FS.Store.FileSystem("jars", {path: Meteor.settings.uploadFolder})]
});

ComponentGroups = new Meteor.Collection("componentGroups");
Devices = new Meteor.Collection("devices");
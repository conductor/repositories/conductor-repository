ComponentGroupRepository = (function() {
    function find(org, name, version) {
        return ComponentGroups.findOne({
            org: org,
            name: name,
            version: version
        });
    }

    function exist(org, name, version) {
        return ComponentGroups.findOne({
            org: org,
            name: name,
            version: version
        }) !== undefined;
    }

    return {
        find: find,
        exist: exist
    }
}());
DeviceRepository = (function() {
    function find(org, name, version) {
        return Devices.findOne({
            org: org,
            name: name,
            version: version
        });
    }

    function exist(org, name, version) {
        return Devices.findOne({
            org: org,
            name: name,
            version: version
        }) !== undefined;
    }

    return {
        find: find,
        exist: exist
    }
}());
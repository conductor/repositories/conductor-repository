angular.module('conductorRepository')
    .controller('search', ['$scope', '$stateParams', '$meteor',
        function($scope, $stateParams, $meteor) {
            $scope.files = JarRepository.find().fetch();
            $scope.searchQuery = '';

            $scope.$watch('query', function() {
                if (!$scope.query) {
                    return;
                }

                $meteor.call('searchDevice', {
                    simpleQuery: $scope.query
                }).then(
                    function(devices) {
                        $scope.devices = devices;
                    },
                    function(error) {
                        alert(error);
                    });

                $meteor.call('searchComponentGroup', {
                    simpleQuery: $scope.query
                }).then(
                    function(componentGroups) {
                        $scope.componentGroups = componentGroups;
                    },
                    function(error) {
                        alert(error);
                    });
            });

            $scope.downloadComponentGroups = function(componentGroups) {
                _.each(componentGroups, function(componentGroup) {
                    $scope.downloadComponentGroup(componentGroup);
                });
            }

            $scope.downloadComponentGroup = function(componentGroup) {
                $meteor.call('downloadURL', componentGroup).then(
                    function(url) {
                        window.open(url, '_blank');
                    },
                    function(error) {
                        alert(error);
                    })
            }
        }
    ]);
angular.module('conductorRepository')
    .controller('addComponentGroup', ['$scope', '$stateParams', '$meteor', 'fileReader', '$sce', '$interval',
        function($scope, $stateParams, $meteor, fileReader, $sce, $interval) {
            $scope.jars = $meteor.collection(JarRepository, false, JarRepository);
            $scope.fileUploads;
            var uploadInterval;

            $scope.upload = {
                progress: undefined,
                file: undefined
            };

            $scope.addJar = function() {
                // Trigger the "file selection" dialog window.
                $('<input type="file">').bind("change", function(event) {
                    // Save the selected file on the server.
                    $scope.jars.save(event.target.files[0]).then(function(data) {
                            if (data == null || data.length == 0) {
                                alert("File upload failed.");
                            } else {
                                // Update the file upload progression.
                                var fileId = data[0]._id._id;
                                $scope.upload.file = fileId;
                                updateUploadProgress(fileId);
                            }
                        },
                        function(error) {
                            alert("File upload failed. Error message: " + error);
                        });
                }).click();
            };

            $scope.getFile = function() {
                fileReader.readAsText($scope.file, $scope).then(
                    function(result) {
                        try {
                            var json = JSON.parse(result);
                            $scope.projectDescriptionHumanReadable = $sce.trustAsHtml(syntaxHighlight(result));
                            $scope.projectDescription = JSON.parse(result);
                        } catch (error) {
                            alert('Failed to parse file as JSON. The error was: ' + error);
                        }
                    },
                    function(error) {
                        alert('An error occued while reading file. The error was: ' + error);
                    });
            };

            function syntaxHighlight(json) {
                json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
                return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function(match) {
                    var cls = 'number';
                    if (/^"/.test(match)) {
                        if (/:$/.test(match)) {
                            cls = 'key';
                        } else {
                            cls = 'string';
                        }
                    } else if (/true|false/.test(match)) {
                        cls = 'boolean';
                    } else if (/null/.test(match)) {
                        cls = 'null';
                    }
                    return '<span class="' + cls + '">' + match + '</span>';
                });
            }

            function updateUploadProgress(fileId) {
                uploadInterval = $interval(function() {
                    // Get upload progress from the meteor server.
                    Meteor.call('getUploadProgress', fileId, function(error, uploadProgress) {
                        // Stop getting the upload progress when the upload is completed.
                        if (uploadProgress == 100) {
                            $interval.cancel(uploadInterval);
                        }

                        $scope.upload.progress = uploadProgress;
                        console.log('Upload progress: ' + uploadProgress + '%');
                    });
                }, 200); // Fetch upload progress every 200ms.
            }

            $scope.addComponentGroup = function() {
                if ($scope.upload.progress !== 100) {
                    alert("You must upload a jar file containing the component group.");
                    return;
                }

                $scope.projectDescription.fileId = $scope.upload.file;

                $meteor.call('addComponentGroup', $scope.projectDescription).then(
                    function(data) {
                        alert('Successfully added component group!');
                    },
                    function(error) {
                        alert('An error occured while adding component group. ' + error.reason);
                    });
            }
        }
    ]);
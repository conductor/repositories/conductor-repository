angular.module('conductorRepository')
    .controller('addDevice', ['$scope', '$stateParams', '$meteor',
        function($scope, $stateParams, $meteor) {
            $scope.device = {
                componentGroups: [{
                    org: "",
                    name: "",
                    version: ""
                }]
            }

            $scope.addComponentGroup = function() {
                $scope.device.componentGroups.push({
                    org: "",
                    name: "",
                    version: ""
                });
            }

            $scope.addDevice = function(device) {
                // Filter out empty component groups
                device.componentGroups = _.filter(device.componentGroups, function(componentGroup) {
                    return componentGroup.org.length !== 0 && componentGroup.name.length !== 0 && componentGroup.version.length !== 0;
                });

                // We need to do this to remove all $$ properties, as they cause an error to be thrown when calling server method
                var newDevice = jQuery.extend(true, {}, device);
                newDevice.componentGroups = _.map(newDevice.componentGroups, function(componentGroup) {
                    delete componentGroup.$$hashKey;
                    return componentGroup;
                })

                $meteor.call('addDevice', newDevice).then(
                    function(data) {
                        alert('Successfully added device!');
                    },
                    function(error) {
                        alert('An error occured while adding device. ' + error.reason);
                    });
            }
        }
    ]);
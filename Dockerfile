FROM ubuntu:14.04

RUN mkdir /home/meteorapp

WORKDIR /home/meteorapp

ADD . ./meteorapp

# Do basic updates
RUN apt-get update -q && apt-get clean

RUN sudo apt-get install -y python2.7
RUN sudo ln -s /usr/bin/python2.7 /usr/bin/python

RUN sudo apt-get install -y build-essential

RUN sudo apt-get install -y git

# Get curl in order to download curl
RUN apt-get install curl -y \

  # Install Meteor
  && echo "Installing Meteor" \
  && (curl https://install.meteor.com/ | sh) \
  && cd /home/meteorapp/meteorapp/ \

  # Npm install
  && echo "Npm install" \
  && meteor npm install \

  # Create folder where the jar uploads will be stored.
  && mkdir /home/meteorapp/uploads \

  # Build the Meteor app
  && echo "Building Meteor app" \
  && meteor build ./build --directory --allow-superuser \

  # Install the version of Node.js we need.
  && echo "Installing node.js" \
  && cd /home/meteorapp/meteorapp/build/bundle \
  && bash -c 'curl "https://nodejs.org/dist/v4.3.1/node-v4.3.1-linux-x64.tar.gz" > /home/meteorapp/meteorapp/build/required-node-linux-x64.tar.gz' \
  && cd /usr/local && tar --strip-components 1 -xzf /home/meteorapp/meteorapp/build/required-node-linux-x64.tar.gz \
  && rm /home/meteorapp/meteorapp/build/required-node-linux-x64.tar.gz \

  # Build the NPM packages needed for build
  && echo "Building NPM packages" \
  && cd /home/meteorapp/meteorapp/build/bundle/programs/server \
  && npm install \

  # Get rid of Meteor. We're done with it.
  && echo "Removing unneccessary meteor files" \
  && rm /usr/local/bin/meteor \
  && rm -rf ~/.meteor \

  #no longer need curl
  && echo "Removing curl" \
  && apt-get --purge autoremove curl -y

RUN npm install -g forever

EXPOSE 80
ENV PORT 80

CMD ["forever", "--minUptime", "1000", "--spinSleepTime", "1000", "meteorapp/build/bundle/main.js"]